# python tif2js.py > ../barra.js

import rasterio
import numpy as np
from math import ceil

# open data
with rasterio.open('barra-dem2.tif') as ds:

	# open the one and only band, transpose to make it x,y not y,x
	data = np.transpose(ds.read()[0])
	
	# get the affine transformation object
	a = ds.transform
	
	# output as JSON
	print "var data = {"
	print "tl: [" + str(ds.bounds.left) + ", " + str(ds.bounds.top) + "],"
	print "bl: [" + str(ds.bounds.left) + ", " + str(ds.bounds.bottom) + "],"
	print "tr: [" + str(ds.bounds.right) + ", " + str(ds.bounds.top) + "],"
	print "br: [" + str(ds.bounds.right) + ", " + str(ds.bounds.bottom) + "],"
	print "resolution: " + str(a[1]) + ","
	print "proj: \"+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +towgs84=446.448,-125.157,542.06,0.15,0.247,0.842,-20.489 +units=m +no_defs\","
	print "getWidth: function(){ return this.data.length; },"
	print "getHeight: function(){ return this.data[0].length; },"
	print "data: ["
	
	for row in data:
		out = []
		for col in row:
			if (col > 0): 
				out.append(str(int(ceil(col - 0.5))))
			elif (col < 0):
				out.append(str(1))
			else:
				out.append(str(0))
		print "[" + ",".join(out) + "],"
	print "]};"