// Load global variables
var map, points, geojson, transformer, graph, myDb, myDb2, markers, routeCollection;

/**
 * Initialise the Map
 */
function initMap(){

	// initialise the database
	initDb();

	// init global arrays
	points = [];
	markers = [];
	
	// set transform between wgs84 and the projection of the dataset
	wgs84 = "+proj=longlat +datum=WGS84 +no_defs";
	transformer = proj4(wgs84, data.proj);
	
	// build the graph for the astar library
	graph = new Graph(data.data);

	// this is a variable that holds the reference to the Leaflet map object
	map = L.map('map').setView([56.98094722327509,-7.493614270972837], 12);	//lat lng

	// this adds the basemap tiles to the map
	L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
		maxZoom: 17,
		attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
	}).addTo(map);
	
	// add the info box (containing buttons) to the map
	addInfoBox();

	// add listener for click event on the map
	map.on('click', onMapClick);
}


/**
 * Initialise the database into the global myDb variable
 */
function initDb() {

	// initialize Firebase (this is copied and pasted from your firebase console)
	var config = {
		apiKey: "AIzaSyCX3L-_REs-xhMLCrbqovtCCHX7Uc2jEMg",
		authDomain: "barra-61fa4.firebaseapp.com",
		databaseURL: "https://barra-61fa4.firebaseio.com",
		projectId: "barra-61fa4",
		storageBucket: "barra-61fa4.appspot.com",
		messagingSenderId: "1045926249626"
	};
	firebase.initializeApp(config);

	// sign in anonymously - this helps stop your database being abused
	firebase.auth().signInAnonymously().catch(function(error) {
		console.log(error.code);
		console.log(error.message);
	});

	// create a global reference to your 'paths' data collection
	myDb = firebase.database().ref().child('paths');
	
	// create a global reference to your 'paths' data collection
	myDb2 = firebase.database().ref().child('comments');
}


/**
 * Create the info box in the top right corner of the map
 */
function addInfoBox(){

	// create a Leaflet control (generic term for anything you add to the map)
	info = L.control();

	// create the info box to update with population figures on hover
	info.onAdd = function (map) {
		
		// add a div to the control with the class info
		this._div = L.DomUtil.create('div', 'info');
		
		// add content to the div
		this._div.innerHTML = "<button type='button' onclick='saveToDb();'>Save Path</button><button type='button' onclick='undo();'>Undo</button><button type='button' onclick='clearMap();'>Clear Map</button>";
		
		// prevent clicks on the div from being propagated to the map
		L.DomEvent.disableClickPropagation(this._div);
		
		//return the div
		return this._div;
	};

	// add the info window to the map
	info.addTo(map);
}


/**
 * Event handler for map click
 */	
function onMapClick(e) {

	// make sure that the click is somewhere that we can go 
	var a = wgs842image([e.latlng.lng, e.latlng.lat]);
	
	if (data.data[a[0]][a[1]]) {
	
		// snap the location to the dataset
		var snap = image2wgs84(wgs842image([e.latlng.lng, e.latlng.lat]));

		//add marker
		markers.push(L.marker(snap).addTo(map));	//!!!! lat lng

		//store the point
		points.push(snap.reverse());

		// calculate the route between all of the points
		getRoute();
		
	} else {
		alert("Sorry, I can't go there");
	}
}


/**
 * Get the route between all of the points in the points array
 */
function getRoute() {

	//need at least two points
	if (points.length > 1) {
	
		// remove the route from the map if already there
		if (geojson) map.removeLayer(geojson);

		// get new route between all points
		var route = [];
		for (var i=0; i < points.length-1; i++) {
			route.push(getPath(points[i], points[i+1]));
		}
		
		// convert the resulting route array into a feature collection 
		routeCollection = turf.featureCollection(route);
		
		// convert to leaflet GeoJSON object and add to the map
		geojson = L.geoJson(routeCollection, {
			style: {
				color: 'red', 
				weight: 12, 
				opacity: .7
			}
		}).addTo(map);

	} else {
		
		//remove any previous geojson layer
		if (geojson) map.removeLayer(geojson);
	}
}


/**
 * Get the path between two points as turf linestring
 */
function getPath(p1, p2) {
	
	//define a route between p1 and p2
	var startPoint = wgs842image(p1);
	var endPoint = wgs842image(p2);

	//get grid location					
	var start = graph.grid[startPoint[0]][startPoint[1]];
	var end = graph.grid[endPoint[0]][endPoint[1]];

	//get the result
	var result = astar.search(graph, start, end);

	//check that it worked, I edited astar.js so that it returns this on fail
	if (result.length === 0) {

		//write to the output div
		alert("Sorry, I couldn't find a route...");

	} else {

		//convert from image space to wgs84 coordinates and return
		var path = [p1];	// init array with start location 
		for (var i = 0; i < result.length; i++) {
			path.push(image2wgs84([result[i].x, result[i].y]).reverse());
		}

		//add route
		return turf.lineString(path);
	}
}


/**
 * convert osgb to image coordinates
 */
function osgb2image(coord) {
	return [ 
		parseInt((coord[0] - data.bl[0]) / data.resolution), 	
		(data.getHeight() - parseInt((coord[1] - data.bl[1]) / data.resolution)) - 1 
	];
}


/**
 * convert image coordinates to osgb
 */
function image2osgb(px) {
	return [
		data.bl[0] + (px[0] * data.resolution), 
		data.bl[1] + ((data.getHeight() - px[1]) * data.resolution)
	];
}


/**
 * transform osgb coords to wgs84
 */
function osgb2wgs84(osgb) {
	var latLng = transformer.inverse(osgb);
	return [ latLng[1], latLng[0] ];
}


/**
 * transform wgs84 coords to osgb
 */
function wgs842osgb(lngLat) {
	return transformer.forward([lngLat[0], lngLat[1]]);
}


/**
 * convenience function to convert from image coordinates to wgs84
 */
function image2wgs84(px) {
	return osgb2wgs84(image2osgb(px));
}


/**
 * convenience function to convert from wgs84 to image coordinates
 */
function wgs842image(lngLat) {
	return osgb2image(wgs842osgb(lngLat));
}


/**
 * Add GeoJSON to database
 */
function saveToDb() {
	
	// don't save if route is empty
	if (geojson) {
	
		//assemble an object to upload
		var out = {		// lng lat
			demographics: demographics,			// objects of demographic info
			comment: document.getElementById("routeCommentBox").value,	//free text comment about the route
			geojson: routeCollection,			// geojson representation of the route	
			points: points, 					// list of points defined by the user	
			time: parseInt(Date.now() / 1000),	// millisecond timestamp for the upload
		};

		// push new data into database object
		myDb.push(out, function(error) {

			// if no error is returned to the callback, then it was loaded successfully
			if (!error) { 
			
				// clear all data from the map and globals
				clearMap();
				
				//report to console
				console.log("successfully added to firebase!");

			// otherwise pass the error to the console and alert user
			} else {
				alert("Data Upload Failed: " + error.message);
				console.error(error.message);
			}
		});
	}
}

/**
 * Clear everything from the map
 */
function clearMap() {
	
	// reset map and globals
	if (geojson) map.removeLayer(geojson);
	for (var m  = 0; m < markers.length; m++) {
		map.removeLayer(markers[m]);
	}
	markers = [];
	points = [];
	route = null;
}


/**
 * Undo the last click
 */
function undo() {

	// is there anything to undo?
	if (markers.length > 1){
	
		// remove the last point from the points array
		points.pop();
	
		//remove marker
		map.removeLayer(markers.pop());
	
		// recalculate the route
		getRoute();
		
	} else if (markers.length === 1) {
	
		// remove the last point from the points array
		points.pop();
	
		// just remove the one remaining marker
		map.removeLayer(markers.pop());
	
	} else {
		
		//in form user nothing to undo
		alert("Nothing to undo!")
	}
}


/**
 * Test all of the transformation functions
 */
function TEST_transforms(tmp0) {
	console.log(tmp0);
	var tmp = image2osgb(tmp0)
	console.log(tmp);
	var tmp2 = osgb2wgs84(tmp);
	console.log(tmp2);
	var tmp3 = wgs842osgb(tmp2);
	console.log(tmp3);
	console.log(osgb2image(tmp3));
}
